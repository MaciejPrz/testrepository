package zadanie1;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Zadadnie2Rowiniecie {
    public static void main(String[] args) {

        boolean isWorking = true;
        Scanner scanner = new Scanner(System.in);

        try (PrintWriter writer = new PrintWriter(new FileWriter("plikform.txt", true))) {
            while (isWorking) {

                System.out.println("imie i nezwisko");
                String imie = scanner.nextLine();
                writer.println(imie);
                if (imie.equals("exit")) {
                    break;
                }

                System.out.println("wiek");
                String wiek = scanner.nextLine();
                writer.println(wiek);

                System.out.println("Płeć");
                String plec = scanner.nextLine();
                writer.println(plec);

                int wiekInt = Integer.parseInt(wiek);

                if (plec.equals("mężczyzna") && wiekInt > 25 && wiekInt < 30) {
                    //kolejne pytanie.
                    System.out.println("Czy płacisz podatki");
                    String dodatkowa = scanner.nextLine();
                    writer.println(dodatkowa);

                    System.out.println("Czy reguralniechodziszz do lekarza");
                    String dodatkowa1 = scanner.nextLine();
                    writer.println(dodatkowa1);

                    System.out.println("Czy masz auto");
                    String dodatkowa2 = scanner.nextLine();
                    writer.println(dodatkowa2);
                }
                if (plec.equals("kobieta")&& wiekInt >18 && wiekInt<25){
                    System.out.println("Czy płacisz podatki");
                    String dodatkowa = scanner.nextLine();
                    writer.println(dodatkowa);

                    System.out.println("Czy reguralniechodziszz do lekarza");
                    String dodatkowa1 = scanner.nextLine();
                    writer.println(dodatkowa1);

                    System.out.println("Czy masz auto");
                    String dodatkowa2 = scanner.nextLine();
                    writer.println(dodatkowa2);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}