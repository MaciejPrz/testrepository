import java.io.*;

public class PrzykladPlik {
    public static void main(String[] args) {

        File plik = new File("plik.txt");

        if (plik.exists()) {
            System.out.println("plik istnieje");

        } else {
            System.out.println("plik nie istnieje");
        }
      //  plik.canRead();
       // plik.createNewFile();
        /*FileReader fileReader = new FileReader("plik.txt");
        BufferedReader reader = new BufferedReader(fileReader);*/
        try (FileReader readerF = new FileReader("plik.txt");
             BufferedReader reader = new BufferedReader(readerF)) {
//            FileReader fileReader = ;

            String linia = null;
            do {
                linia = reader.readLine();

                System.out.println("Linia: " + linia);
            } while (linia != null);

        } catch (FileNotFoundException fnfe) {
            System.out.println("Nie ma takiego pliku.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
